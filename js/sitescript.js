$(document).ready(function(){	
    
    
    $('.menu__item_parent .menu__link').click(function(e) {
        $(this).parent('.menu__item_parent').toggleClass('menu__item_opened');
        return false;
     });
    
   
      
      var highestBox = 0;      
      $('.eq-height', this).each(function(){        
        if($(this).innerHeight() > highestBox) {
          highestBox = $(this).innerHeight(); 
        }
      });
      $('.eq-height',this).css('min-height',highestBox);
    
    
    
    $('#container').bind('heightChange', function(){
        var highestBox = 0;      
          $('.eq-height', this).each(function(){  
            $(this).css('min-height','initial');
            if($(this).innerHeight() > highestBox) {
              highestBox = $(this).innerHeight(); 
            }
          });
          $('.eq-height',this).css('min-height',highestBox);
    });
    
    
    
    /*block-version equial height*/
     var highestBox = 0;      
      $('.block-version-info', this).each(function(){        
        if($(this).innerHeight() > highestBox) {
          highestBox = $(this).innerHeight(); 
        }
      });
     $('.block-version-info',this).css('height',highestBox);
    /*block-version equial height*/
    
    /*fancybox*/
    $("[data-fancybox]").fancybox({
		// Options will go here
	});
    
    
    $('.td-show-more').click(function(e) {        
        
        $(this).parents('tr').toggleClass('tr-opened');
        
        var order=$(this).attr('data-order');
        $('.sub-level[data-order="'+order+'"]').toggleClass('hidden');
        
        $("#container").trigger('heightChange');
        return false;
     });

    /* custom-input - iCheck*/
     $('.custom-input').iCheck({
        checkboxClass: 'icheckbox_minimal',
        radioClass: 'iradio_minimal',
        increaseArea: '20%' // optional
      });

    $('input[type="radio"]').on('ifChecked', function(event){
    	var self = $(this);

    	self.parents('.choose-product__item')
    		.addClass('choose-product__item--border-blue');

    	self.parent()
    		.siblings()
    		.addClass('text-dark');

    	self.parents('.tariff-radio')
    		.siblings()
    		.addClass('tariff-radio-info--text-dark');
	});

	$('input[type="radio"]').on('ifUnchecked', function(event){
		var self = $(this);

		self.parents('.choose-product__item')
			.removeClass('choose-product__item--border-blue');

		self.parent()
			.siblings()
			.removeClass('text-dark');

		self.parents('.tariff-radio')
			.siblings()
			.removeClass('tariff-radio-info--text-dark');
	});
	/* custom-input - iCheck*/

$('.support-topic-short').click(function() {
        $(this).next().slideToggle();
});
    
if ($('.select-field').length){
			$('select').each(function() {
				$('select').styler();
			 $(".jq-selectbox__dropdown").mCustomScrollbar({
    		documentTouchScroll: true
    	});
			});
}
				
	$('.file').change(function(){
		if( $(this).val().length < 35 ) {
			$('#filetext').html($(this).val());
		} else {
			$('#filetext').html($(this).val().substring(0,25) + '...');
		}
		
		});
		if ($('.phone').length){
			$('.phone').mask("+7 (999) 999-99-99");
		}

if ($('.graph-general').length){
	
	Highcharts.setOptions({
            lang: {
                loading: 'Загрузка...',
                months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
                shortMonths: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
                exportButtonTitle: "Экспорт",
                printButtonTitle: "Печать",
                rangeSelectorFrom: "С",
                rangeSelectorTo: "По",
                rangeSelectorZoom: "Период",
                downloadPNG: 'Скачать PNG',
                downloadJPEG: 'Скачать JPEG',
                downloadPDF: 'Скачать PDF',
                downloadSVG: 'Скачать SVG',
                printChart: 'Напечатать график'
            },
												
												chart: {
        type: 'line',
        backgroundColor: '#ffffff',
        plotBackgroundColor: '#f8f9fa'
    },
    title:{  text: '' },
    subtitle: {  text: '' },
    exporting: { enabled: false },
    credits: { enabled: false },
    xAxis: {
        type: 'datetime',
							 gridLineWidth: 1,
        labels: {
          formatter: function() {
            return Highcharts.dateFormat("%B'%y", this.value);
          }
        },
							tickColor: '#e5e9ee',
								minTickInterval: 3600*24*30*1000,//time in milliseconds
        minRange: 3600*24*30*1000,
        ordinal: false //this sets the fixed time formats  
    },
    yAxis: {
         title: {
		       text: ''
		     },
         min: 0,
         
    },
        legend: {
            align: 'left'
        },
    tooltip: {
        headerFormat: '<b>{series.name}</b><br>',
        pointFormat: '{point.x:%e  %b}: {point.y:.2f} '
    },

    plotOptions: {
        line: {
            marker: {
                enabled: false
            },
            color: '#008dcd'
        }
    }
    });

		
				
    Highcharts.chart('graph01', {
					 plotOptions: {
        line: {
            color: '#bb2433'
        }
      },
      series: [{
        name: 'Общее количество участников',
        data: [
            [Date.UTC(2016, 0, 1), 1250000],
            [Date.UTC(2016, 0, 2), 1200000],
            [Date.UTC(2016, 0, 5), 1400000],
            [Date.UTC(2016, 0, 8), 1320000],
            [Date.UTC(2016, 0, 9), 1415000],
            [Date.UTC(2016, 0, 20), 1305000],
            [Date.UTC(2016, 0, 23), 1530000],
            [Date.UTC(2016, 0, 29), 1295000],
            [Date.UTC(2016, 0, 31), 1305000],
            [Date.UTC(2016, 1, 2), 1800000],
            [Date.UTC(2016, 1, 6), 1300000],
            [Date.UTC(2016, 1, 7), 1480000],
            [Date.UTC(2016, 1, 9), 1490000],
            [Date.UTC(2016, 1, 10), 1400000],
            [Date.UTC(2016, 1, 13), 1420000],
            [Date.UTC(2016, 1, 19), 1400000],
            [Date.UTC(2016, 1, 23), 1300000],
            [Date.UTC(2016, 1, 25), 1350000],
            [Date.UTC(2016, 2, 3), 1350000],
            [Date.UTC(2016, 2, 5), 1400000],
            [Date.UTC(2016, 2, 9), 1290000],
            [Date.UTC(2016, 2, 12), 1280000],
            [Date.UTC(2016, 2, 13), 1495000],
            [Date.UTC(2016, 2, 15), 1500000],
            [Date.UTC(2016, 2, 16), 1300000],
            [Date.UTC(2016, 2, 18), 1310000],
            [Date.UTC(2016, 2, 20), 1280000],
            [Date.UTC(2016, 2, 24), 1390000],
            [Date.UTC(2016, 2, 26), 1350000],
            [Date.UTC(2016, 3, 1), 1340000],
            [Date.UTC(2016, 3, 2), 1200000],
            [Date.UTC(2016, 3, 4), 1250000],
            [Date.UTC(2016, 3, 5), 1400000],
            [Date.UTC(2016, 3, 7), 1230000],
            [Date.UTC(2016, 3, 8), 1240000],
            [Date.UTC(2016, 3, 11), 1230000],
            [Date.UTC(2016, 3, 12), 1210000],
            [Date.UTC(2016, 3, 17), 1280000],
            [Date.UTC(2016, 3, 22), 800000],
            [Date.UTC(2016, 3, 28), 1280000],
            [Date.UTC(2016, 3, 30), 1250000]
        ]
      }]
				});
				
				Highcharts.chart('graph02', {
      series: [{
        name: 'Количество новых участников',
        data: [
            [Date.UTC(2016, 0, 1), 1250000],
            [Date.UTC(2016, 0, 2), 1200000],
            [Date.UTC(2016, 0, 5), 1400000],
            [Date.UTC(2016, 0, 8), 1320000],
            [Date.UTC(2016, 0, 9), 1415000],
            [Date.UTC(2016, 0, 20), 1305000],
            [Date.UTC(2016, 0, 23), 1530000],
            [Date.UTC(2016, 0, 29), 1295000],
            [Date.UTC(2016, 0, 31), 1305000],
            [Date.UTC(2016, 1, 2), 1800000],
            [Date.UTC(2016, 1, 6), 1300000],
            [Date.UTC(2016, 1, 7), 1480000],
            [Date.UTC(2016, 1, 9), 1490000],
            [Date.UTC(2016, 1, 10), 1400000],
            [Date.UTC(2016, 1, 13), 1420000],
            [Date.UTC(2016, 1, 19), 1400000],
            [Date.UTC(2016, 1, 23), 1300000],
            [Date.UTC(2016, 1, 25), 1350000],
            [Date.UTC(2016, 2, 3), 1350000],
            [Date.UTC(2016, 2, 5), 1400000],
            [Date.UTC(2016, 2, 9), 1290000],
            [Date.UTC(2016, 2, 12), 1280000],
            [Date.UTC(2016, 2, 13), 1495000],
            [Date.UTC(2016, 2, 15), 1500000],
            [Date.UTC(2016, 2, 16), 1300000],
            [Date.UTC(2016, 2, 18), 1310000],
            [Date.UTC(2016, 2, 20), 1280000],
            [Date.UTC(2016, 2, 24), 1390000],
            [Date.UTC(2016, 2, 26), 1350000],
            [Date.UTC(2016, 3, 1), 1340000],
            [Date.UTC(2016, 3, 2), 1200000],
            [Date.UTC(2016, 3, 4), 1250000],
            [Date.UTC(2016, 3, 5), 1400000],
            [Date.UTC(2016, 3, 7), 1230000],
            [Date.UTC(2016, 3, 8), 1240000],
            [Date.UTC(2016, 3, 11), 1230000],
            [Date.UTC(2016, 3, 12), 1210000],
            [Date.UTC(2016, 3, 17), 1280000],
            [Date.UTC(2016, 3, 22), 800000],
            [Date.UTC(2016, 3, 28), 1280000],
            [Date.UTC(2016, 3, 30), 1250000]
        ]
      }]
				});
				
				Highcharts.chart('graph03', {
      series: [{
        name: 'Общее количество',
        data: [
            [Date.UTC(2016, 0, 1), 1250000],
            [Date.UTC(2016, 0, 2), 1200000],
            [Date.UTC(2016, 0, 5), 1400000],
            [Date.UTC(2016, 0, 8), 1320000],
            [Date.UTC(2016, 0, 9), 1415000],
            [Date.UTC(2016, 0, 20), 1305000],
            [Date.UTC(2016, 0, 23), 1530000],
            [Date.UTC(2016, 0, 29), 1295000],
            [Date.UTC(2016, 0, 31), 1305000],
            [Date.UTC(2016, 1, 2), 1800000],
            [Date.UTC(2016, 1, 6), 1300000],
            [Date.UTC(2016, 1, 7), 1480000],
            [Date.UTC(2016, 1, 9), 1490000],
            [Date.UTC(2016, 1, 10), 1400000],
            [Date.UTC(2016, 1, 13), 1420000],
            [Date.UTC(2016, 1, 19), 1400000],
            [Date.UTC(2016, 1, 23), 1300000],
            [Date.UTC(2016, 1, 25), 1350000],
            [Date.UTC(2016, 2, 3), 1350000],
            [Date.UTC(2016, 2, 5), 1400000],
            [Date.UTC(2016, 2, 9), 1290000],
            [Date.UTC(2016, 2, 12), 1280000],
            [Date.UTC(2016, 2, 13), 1495000],
            [Date.UTC(2016, 2, 15), 1500000],
            [Date.UTC(2016, 2, 16), 1300000],
            [Date.UTC(2016, 2, 18), 1310000],
            [Date.UTC(2016, 2, 20), 1280000],
            [Date.UTC(2016, 2, 24), 1390000],
            [Date.UTC(2016, 2, 26), 1350000],
            [Date.UTC(2016, 3, 1), 1340000],
            [Date.UTC(2016, 3, 2), 1200000],
            [Date.UTC(2016, 3, 4), 1250000],
            [Date.UTC(2016, 3, 5), 1400000],
            [Date.UTC(2016, 3, 7), 1230000],
            [Date.UTC(2016, 3, 8), 1240000],
            [Date.UTC(2016, 3, 11), 1230000],
            [Date.UTC(2016, 3, 12), 1210000],
            [Date.UTC(2016, 3, 17), 1280000],
            [Date.UTC(2016, 3, 22), 800000],
            [Date.UTC(2016, 3, 28), 1280000],
            [Date.UTC(2016, 3, 30), 1250000]
        ]
      }]
				});
				
				Highcharts.chart('graph04', {
      series: [{
        name: 'Количество открытых циклов',
        data: [
            [Date.UTC(2016, 0, 1), 1250000],
            [Date.UTC(2016, 0, 2), 1200000],
            [Date.UTC(2016, 0, 5), 1400000],
            [Date.UTC(2016, 0, 8), 1320000],
            [Date.UTC(2016, 0, 9), 1415000],
            [Date.UTC(2016, 0, 20), 1305000],
            [Date.UTC(2016, 0, 23), 1530000],
            [Date.UTC(2016, 0, 29), 1295000],
            [Date.UTC(2016, 0, 31), 1305000],
            [Date.UTC(2016, 1, 2), 1800000],
            [Date.UTC(2016, 1, 6), 1300000],
            [Date.UTC(2016, 1, 7), 1480000],
            [Date.UTC(2016, 1, 9), 1490000],
            [Date.UTC(2016, 1, 10), 1400000],
            [Date.UTC(2016, 1, 13), 1420000],
            [Date.UTC(2016, 1, 19), 1400000],
            [Date.UTC(2016, 1, 23), 1300000],
            [Date.UTC(2016, 1, 25), 1350000],
            [Date.UTC(2016, 2, 3), 1350000],
            [Date.UTC(2016, 2, 5), 1400000],
            [Date.UTC(2016, 2, 9), 1290000],
            [Date.UTC(2016, 2, 12), 1280000],
            [Date.UTC(2016, 2, 13), 1495000],
            [Date.UTC(2016, 2, 15), 1500000],
            [Date.UTC(2016, 2, 16), 1300000],
            [Date.UTC(2016, 2, 18), 1310000],
            [Date.UTC(2016, 2, 20), 1280000],
            [Date.UTC(2016, 2, 24), 1390000],
            [Date.UTC(2016, 2, 26), 1350000],
            [Date.UTC(2016, 3, 1), 1340000],
            [Date.UTC(2016, 3, 2), 1200000],
            [Date.UTC(2016, 3, 4), 1250000],
            [Date.UTC(2016, 3, 5), 1400000],
            [Date.UTC(2016, 3, 7), 1230000],
            [Date.UTC(2016, 3, 8), 1240000],
            [Date.UTC(2016, 3, 11), 1230000],
            [Date.UTC(2016, 3, 12), 1210000],
            [Date.UTC(2016, 3, 17), 1280000],
            [Date.UTC(2016, 3, 22), 800000],
            [Date.UTC(2016, 3, 28), 1280000],
            [Date.UTC(2016, 3, 30), 1250000]
        ]
      }]
				});
				
				Highcharts.chart('graph05', {
      series: [{
        name: 'Количество закрытых циклов',
        data: [
            [Date.UTC(2016, 0, 1), 1250000],
            [Date.UTC(2016, 0, 2), 1200000],
            [Date.UTC(2016, 0, 5), 1400000],
            [Date.UTC(2016, 0, 8), 1320000],
            [Date.UTC(2016, 0, 9), 1415000],
            [Date.UTC(2016, 0, 20), 1305000],
            [Date.UTC(2016, 0, 23), 1530000],
            [Date.UTC(2016, 0, 29), 1295000],
            [Date.UTC(2016, 0, 31), 1305000],
            [Date.UTC(2016, 1, 2), 1800000],
            [Date.UTC(2016, 1, 6), 1300000],
            [Date.UTC(2016, 1, 7), 1480000],
            [Date.UTC(2016, 1, 9), 1490000],
            [Date.UTC(2016, 1, 10), 1400000],
            [Date.UTC(2016, 1, 13), 1420000],
            [Date.UTC(2016, 1, 19), 1400000],
            [Date.UTC(2016, 1, 23), 1300000],
            [Date.UTC(2016, 1, 25), 1350000],
            [Date.UTC(2016, 2, 3), 1350000],
            [Date.UTC(2016, 2, 5), 1400000],
            [Date.UTC(2016, 2, 9), 1290000],
            [Date.UTC(2016, 2, 12), 1280000],
            [Date.UTC(2016, 2, 13), 1495000],
            [Date.UTC(2016, 2, 15), 1500000],
            [Date.UTC(2016, 2, 16), 1300000],
            [Date.UTC(2016, 2, 18), 1310000],
            [Date.UTC(2016, 2, 20), 1280000],
            [Date.UTC(2016, 2, 24), 1390000],
            [Date.UTC(2016, 2, 26), 1350000],
            [Date.UTC(2016, 3, 1), 1340000],
            [Date.UTC(2016, 3, 2), 1200000],
            [Date.UTC(2016, 3, 4), 1250000],
            [Date.UTC(2016, 3, 5), 1400000],
            [Date.UTC(2016, 3, 7), 1230000],
            [Date.UTC(2016, 3, 8), 1240000],
            [Date.UTC(2016, 3, 11), 1230000],
            [Date.UTC(2016, 3, 12), 1210000],
            [Date.UTC(2016, 3, 17), 1280000],
            [Date.UTC(2016, 3, 22), 800000],
            [Date.UTC(2016, 3, 28), 1280000],
            [Date.UTC(2016, 3, 30), 1250000]
        ]
      }]
				});
}
	
	/* SIDEBAR MENU HOVER*/
	
	/*	var timer;
		$('.sidebar').hover(
		  function(e) {
			timer = setTimeout(function () {
					$('.sidebar').addClass('sidebar_hover');
					$('.sidebar__inner').addClass('sidebar__inner_hover');
					$('#opacity').show().css('opacity', '1');
				}, 300);
			},function(e) {
				clearTimeout(timer);
				$(this).removeClass('sidebar_hover');
				$(this).find('.sidebar__inner').removeClass('sidebar__inner_hover');
				$('#opacity').hide().css('opacity', '0');
				$('.menu-item').removeClass('menu-item_active');
				$('.submenu').removeClass('submenu_active');
			}
		);
		
		$('.toggle-sidebar__icon-container').hover(
			function(e) {
				$(this).parents('.toggle-sidebar').find('.toggle-sidebar__caption').addClass('toggle-sidebar__caption_active');
				if($('.slider-product').length){
					$('.slider-product').slick('refresh');	
				}
			},function(e) {
				$(this).parents('.toggle-sidebar').find('.toggle-sidebar__caption').removeClass('toggle-sidebar__caption_active');
				if($('.slider-product').length){
					$('.slider-product').slick('refresh');	
				}
			}
		);
		
	
		
		
		
		$('.menu-item').hover(
			function(e) {
				$('.menu-item').removeClass('menu-item_active');
				$('.submenu').removeClass('submenu_active');
				$(this).addClass('menu-item_active');
				var st=145+parseInt($('.sidebar__inner').css('top'));
				$(this).find('.submenu').css('top', st);
				$('.menu-item_active').find('.submenu').addClass('submenu_active');			
			},function(e) {
				
				if(!$('.toggle-sidebar__icon-container').is(":hover")){ 
				            
					$('.submenu').removeClass('submenu_active');
				}
			}
		);
	
	
	$('.right-sidebar').stick_in_parent();
	
	/*
	
	$('#category-description-paste').html($('#category-description').html());
	$('#category-description').empty();
	
	if ($(".category-inner_sidebar").length) {
		
		$('.subcategory-item__link').click(function(e) {
            var target=$('#'+$(this).attr('data-type'));						
			tt = target.offset().top-20;
            $('html, body').animate({
                scrollTop: tt
            }, 1500);
			return false;
        });
		
		
		$('.category-inner_sidebar .products-category-block').each(function(){
				if ($(window).scrollTop() >= $(this).offset().top-60 && $(this).scrollTop() <= $(this).offset().top + $(this).height()-60) {
					$('.subcategory-list .subcategory-item').removeClass('subcategory-item_active');
					el_id=$(this).attr('id');	
					$("a[data-type='" + el_id + "']").parent('.subcategory-item').addClass('subcategory-item_active');					
				}
				
			});	
		}

		
		$(window).scroll(function(){
			if ($(".category-inner_sidebar").length) {
			$('.category-inner_sidebar .products-category-block').each(function(){
				if ($(window).scrollTop() >= $(this).offset().top-60 && $(this).scrollTop() <= $(this).offset().top + $(this).height()-60) {
					$('.subcategory-list .subcategory-item').removeClass('subcategory-item_active');
					el_id=$(this).attr('id');					
					$("a[data-type='" + el_id + "']").parent('.subcategory-item').addClass('subcategory-item_active');					
				}
				
			});				
		}

	});
	
	
	
	
	$('.slider-product').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: false			
	});
	
	$('.slider-product-thumbnails li').each(function(i) {
		$('.slider-product-thumbnails .slide-' + i + '').click(function(){
			$('.slider-product-thumbnails li').removeClass('active');
			$(this).addClass('active');
			$('.slider-product').slick('slickGoTo', parseInt(i) );
			return false;
		 });
	});
	$('.slider-product-thumbnails li').removeClass('active');
	$('.slider-product-thumbnails li').eq(0).addClass('active');
	$('.slider-product').on('afterChange', function (event, slick, currentSlide, nextSlide) {
		$('.slider-product-thumbnails li').removeClass('active');
		$('.slider-product-thumbnails .slide-' + currentSlide + '').addClass('active');			
	});
	
	
	$('.show-more-characteristics').click(function(e) {
        $('.product-table tr').show();
		$(this).hide();
		return false;
    });
	
	//Range price slider	
	if(document.getElementById('range') !== null){	
		var range = document.getElementById('range');
		noUiSlider.create(range, {
			start: [ 1000, 223000 ], // Handle start position
			connect:true,
			step: 1, // Slider moves in increments of '10'
			margin: 20, // Handles must be more than '20' apart		
			range: { // Slider can select '0' to '100'
				'min': 0,
				'max': 300000
			}
		});
		
		var valueInput = document.getElementById('value-input');
		var valueInput2 = document.getElementById('value-input-2');
	
		range.noUiSlider.on('update', function( values, handle ) {
			if ( handle ) {
				valueInput2.value = Math.round(values[handle]) + ' Р.';
			} else {
				valueInput.value = Math.round(values[handle]) + ' Р.';
			}
		});
		
		valueInput.addEventListener('change', function(){
			range.noUiSlider.set([parseInt(this.value), null]);
		});
		
		valueInput2.addEventListener('change', function(){
			range.noUiSlider.set([null, parseInt(this.value)]);
		});
	
	}
	
	
	if(document.getElementById('range2') !== null){
	
		var range = document.getElementById('range2');		
		
		noUiSlider.create(range, {
			start: [ 64, 350 ], // Handle start position
			connect:true,
			step: 1, // Slider moves in increments of '10'
			margin: 20, // Handles must be more than '20' apart		
			range: { // Slider can select '0' to '100'
				'min': 0,
				'max': 500
			}
		});
		
		var valueInput3 = document.getElementById('value-input-3');
		var valueInput4 = document.getElementById('value-input-4');
	
		range.noUiSlider.on('update', function( values, handle ) {
			if ( handle ) {
				valueInput4.value = Math.round(values[handle]);
			} else {
				valueInput3.value = Math.round(values[handle]);
			}
		});
		
		valueInput3.addEventListener('change', function(){
			range.noUiSlider.set([parseInt(this.value), null]);
		});
		
		valueInput4.addEventListener('change', function(){
			range.noUiSlider.set([null, parseInt(this.value)]);
		});
	
	}
	
	
	$('.filter-toggle__label').click(function(e) {
		if($(this).hasClass('filter-toggle__label_active')){
        	$(this).parent('.filter-toggle').find('.filter-toggle__content').slideUp();
			$(this).toggleClass('filter-toggle__label_active');
		}else{
			$(this).parent('.filter-toggle').find('.filter-toggle__content').slideDown();
			$(this).toggleClass('filter-toggle__label_active');
		}
    });

	
	   
	/*$('input, textarea').each(function(){
 		var placeholder = $(this).attr('placeholder');
 		$(this).focus(function(){ $(this).attr('placeholder', '');});
 		$(this).focusout(function(){			 
 			$(this).attr('placeholder', placeholder);  			
 		});
 	});
	$('.qty-plus').click(function(e) {
		var inp= $(this).parent('div').find('input');
        inp.val(parseInt(inp.val())+1);
    });
	$('.qty-minus').click(function(e) {
		var inp= $(this).parent('div').find('input');	
		val=parseInt(inp.val())-1;	
		if(val<2){ val=1;}
        inp.val(val);
    });
	
		$('.show-text').click(function(e) {
			var txt=$(this).parent('div').find('.text-hidden');
            if(txt.css('display')=='none'){
				txt.slideDown();	
				$(this).find('.show').hide();
				$(this).find('.hide').show();
			}else{
				txt.slideUp();	
				$(this).find('.show').show();
				$(this).find('.hide').hide();
			}
			return false;
        });

	$('#phone').mask("+7(999)999-99-99");
	$('#orderForm').submit(function() { 	
		var error = 0;			
		var emailTest= /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,6})+$/;
		var lastName = $('#lastName', this);
		var firstName = $('#firstName', this);
		var middleName = $('#middleName', this);
		var email = $('#email', this);
		var phone = $('#phone', this);
		
		$(this).find('.input').removeClass('input-error');
		
		if (lastName.val() == ''){
			$(lastName).addClass('input-error');
			$(lastName).attr('placeholder', 'Введите Фамилию!');
			error++;
		}	
		if (firstName.val() == ''){
			$(firstName).addClass('input-error');
			$(firstName).attr('placeholder', 'Введите Имя!');
			error++;
		}	
		if (middleName.val() == ''){
			$(middleName).addClass('input-error');
			$(middleName).attr('placeholder', 'Введите Отчество!');
			error++;
		}
		if (email.val() == ''){
			$(email).addClass('input-error');
			$(email).attr('placeholder', 'Введите E-mail!');
			error++;
		}else if(!emailTest.test(email.val())){
			$(email).addClass('input-error');		
			$(email).attr('placeholder', 'Введите верный E-mail!');			
			error++;
		}
		if (phone.val() == ''){
			$(phone).addClass('input-error');
			$(phone).attr('placeholder', 'Введите Телефон!');
			error++;
		}
		if (error == 0) {
			return true;
			
		}
		return false;
	});

*/
});



 
